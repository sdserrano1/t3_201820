package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T>{

	private Nodo<T> primerNodo;
	
	private int cantidadElem;
	
	public Stack()
	{
		primerNodo = null;
		cantidadElem = 0;			
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorLista<>(primerNodo);
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(cantidadElem == 0)
		{
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return cantidadElem;
	}

	@Override
	public void push(T t) {
		// TODO Auto-generated method stub
		Nodo<T> agregar = new Nodo<T>( t );
        if( primerNodo == null )
        {
            primerNodo = agregar;
        }
        else
        {
            primerNodo.cambiarAnterior(agregar);
            agregar.cambiarSiguiente(primerNodo);
            primerNodo = agregar;
        }
        cantidadElem++;
		
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		Nodo<T> actual = primerNodo;
		T eliminar = null;
        if(primerNodo != null)
        {
        	if(actual.darSiguiente() != null)
        	{
            actual = actual.darSiguiente();            
            primerNodo = actual;
            eliminar = actual.darAnterior().darElemento();
            actual.cambiarAnterior(null);
            cantidadElem--;
            return eliminar;
        	}
        	else
        	{
        		eliminar = primerNodo.darElemento();
        		primerNodo = null;
        		cantidadElem--;
        		return eliminar;
        	}
        }
        return eliminar;
	}

	
}
