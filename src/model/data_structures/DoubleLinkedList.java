package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T> implements IDoublyLinkedList<T>{
	
	Nodo<T> primerNodo;
	Nodo<T> ultimoNodo;
	int cantidadDeElementos;
	
	public DoubleLinkedList() {
		primerNodo=null;
		ultimoNodo=null;
		cantidadDeElementos=0;
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorLista<T>(primerNodo);
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return cantidadDeElementos;
	}

	@Override
	public void add(T elemento) {
		// TODO Auto-generated method stub
		Nodo<T> nodo = new Nodo<T>( elemento );
        if( primerNodo == null )
        {
            primerNodo = nodo;
            ultimoNodo = nodo;
        }
        else
        {
            primerNodo.cambiarAnterior(nodo);
            nodo.cambiarSiguiente(primerNodo);
            primerNodo = nodo;
        }
        cantidadDeElementos++;
	}

	@Override
	public void addAtEnd(T elemento) {
		// TODO Auto-generated method stub
		Nodo<T> nodo = new Nodo<T>( elemento );
        if( primerNodo == null )
        {
            primerNodo = nodo;
            ultimoNodo = nodo;
        }
        else
        {
            ultimoNodo.cambiarSiguiente(nodo);
            nodo.cambiarAnterior(ultimoNodo);
            ultimoNodo = nodo;
        }
        cantidadDeElementos++;
	}

	@Override
	public void addAtK(T elemento,int pos) {
		// TODO Auto-generated method stub
		Nodo<T> nodo = new Nodo<T>( elemento );
        if( ( pos >= cantidadDeElementos ) || pos < 0 )
        {
            throw new IndexOutOfBoundsException("El lugar donde se quiere agregar el elemento esta fuera de la lisa pos:"+pos);
        }
        else
        {
            Nodo<T> aux = primerNodo;
            
            for( int cont = 0; cont < pos - 1; cont++ )
            {
                aux = aux.darSiguiente( );
            }
            nodo.cambiarAnterior(aux);
            nodo.cambiarSiguiente(aux.darSiguiente());
            aux.darSiguiente().cambiarAnterior(nodo);
            aux.cambiarSiguiente(nodo);
            cantidadDeElementos++;
        }

	}

	@Override
	public T getElement(int pos) {
		// TODO Auto-generated method stub
        if( pos >= cantidadDeElementos || pos < 0 )
        {
        	throw new IndexOutOfBoundsException("El lugar de donde se quiere sacar el elemento esta fuera de la lisa pos:"+pos);
        }
        else
        {
            Nodo<T> aux = primerNodo;

            for( int cont = 0; cont < pos; cont++ )
            {
                aux = aux.darSiguiente( );
            }
            
            return aux.darElemento( );
        }
	}

	@Override
	public T getCurrentElement() {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public T delete() {
		// TODO Auto-generated method stub
		T valor=null;
		if(primerNodo!=null){
			valor=primerNodo.darElemento();
			primerNodo.darSiguiente().cambiarAnterior(null);
			primerNodo=primerNodo.darSiguiente();
			cantidadDeElementos--;
		}
		return valor;
	}

	@Override
	public T deleteAtK(int pos) {
		// TODO Auto-generated method stub
        T valor = null;

        if( ( pos >= cantidadDeElementos ) || pos < 0 )
        {
        	throw new IndexOutOfBoundsException("El lugar de donde se quiere sacar el elemento esta fuera de la lisa pos:"+pos);
        }
        else if( pos == 0 )
        {
            if( primerNodo.equals( ultimoNodo ) )
            {
                ultimoNodo = null;
            }
            valor = primerNodo.darElemento( );
            primerNodo = null;
            cantidadDeElementos--;
            return valor;
        }
        else
        {

            Nodo<T> p = primerNodo.darSiguiente( );
            for( int cont = 1; cont < pos; cont++ )
            {
                p = p.darSiguiente( );
            }
            valor = p.darElemento( );
            if( p.equals( ultimoNodo ) )
            {
            	ultimoNodo = p.darAnterior( );
            	ultimoNodo.cambiarSiguiente(null);
            }else{
            	p.darAnterior().cambiarSiguiente(p.darSiguiente());
            	p.darSiguiente().cambiarAnterior(p.darAnterior());
            }
            cantidadDeElementos--;
            return valor;
        }
	}

	@Override
	public Nodo<T> next(Nodo<T> nodo) {
		// TODO Auto-generated method stub
		return nodo.darSiguiente();
	}

	@Override
	public Nodo<T> previous(Nodo<T> nodo) {
		// TODO Auto-generated method stub
		return nodo.darAnterior();
	}

}
