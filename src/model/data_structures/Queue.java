package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T> {
	
	int cantidadElementos;
	
	Nodo<T> primerNodo;
	
	Nodo<T> ultimoNodo;
	
	public Queue(){
		primerNodo=null;
		ultimoNodo=null;
		cantidadElementos=0;
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorLista<T>(primerNodo);
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(cantidadElementos==0){
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return cantidadElementos;
	}

	@Override
	public void enqueue(T t) {
		// TODO Auto-generated method stub
		Nodo<T> nuevo = new Nodo<T>(t);
		if(primerNodo!=null){
			ultimoNodo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimoNodo);
			ultimoNodo=nuevo;
		}
		else{
			primerNodo=nuevo;
			ultimoNodo=nuevo;
		}
		cantidadElementos++;
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		Nodo<T> actual = primerNodo;
		T eliminar = null;
        if(primerNodo != null)
        {
        	if(actual.darSiguiente() != null)
        	{
            actual = actual.darSiguiente();            
            primerNodo = actual;
            eliminar = actual.darAnterior().darElemento();
            actual.cambiarAnterior(null);
            cantidadElementos--;
            return eliminar;
        	}
        	else
        	{
        		eliminar = primerNodo.darElemento();
        		primerNodo = null;
        		cantidadElementos--;
        		return eliminar;
        	}
        }
        return eliminar;
	}

}
