package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {

	//TODO Agregar API de DoublyLinkedList
	Integer getSize();
	
	public void add(T elemento);
	
	public void addAtEnd(T elemento);
	
	public void addAtK(T elemento, int pos);
	
	public T getElement(int pos);
	
	public T getCurrentElement();
	
	public T delete();
	
	public T deleteAtK(int pos);
	
	public Nodo<T> next(Nodo<T> nodo);
	
	public Nodo<T> previous(Nodo<T> nodo);
}
