package model.vo;

import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip {
	
	int tripId;
	
	int bikeId;
	
	double tripDuration;
	
	int from_Station_Id;
	
	String from_Station_Name;
	
	int to_Station_Id;
	
	String to_Station_Name;
	
	String usertype;
	
	String gender;
	
	int birthyear;
	
	public VOTrip(int tripId,int bikeId,double tripDuration,int from_Station_Id,String from_Station_Name,int to_Station_Id,String to_Station_Name,String gender) {
		// TODO Auto-generated constructor stub
		this.tripId=tripId;
		this.bikeId=bikeId;
		this.tripDuration=tripDuration;
		this.from_Station_Id=from_Station_Id;
		this.from_Station_Name=from_Station_Name;
		this.to_Station_Id=to_Station_Id;
		this.to_Station_Name=to_Station_Name;
		this.gender=gender;
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return tripDuration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return from_Station_Name;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return to_Station_Name;
	}
	
	public String getGender(){
		return gender;
	}
	
	public int getToStationId(){
		return to_Station_Id;
	}
	
	public int getBikeId(){
		return bikeId;
	}
}
