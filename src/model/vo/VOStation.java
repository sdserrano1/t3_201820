package model.vo;

public class VOStation {
	int id;
	
	String name;
	
	String city;
	
	double latitude;
	
	double longitude;
	
	int dpcapacity;
	
	public VOStation(int id,String name,String city,double latitude,double longitude,int dpcapacity){
		this.id=id;
		this.name=name;
		this.city=city;
		this.latitude=latitude;
		this.longitude=longitude;
		this.dpcapacity=dpcapacity;
	}
	
	public int getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public String getCity(){
		return city;
	}
	
	public double getLatitude(){
		return latitude;
	}
	
	public double getLongitude(){
		return longitude;
	}
	
	public double getDpcapacity(){
		return dpcapacity;
	}
}
