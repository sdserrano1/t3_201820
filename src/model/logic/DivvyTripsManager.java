package model.logic;

import java.io.FileReader;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager {
	
	DoubleLinkedList<VOStation> listaEstaciones=new DoubleLinkedList<>();
	
	Queue<VOTrip> listaTripsQueue = new Queue<>();
	
	Stack<VOTrip> listaTripsStack = new Stack<>();
	
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		try{
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
		     String [] nextLine;
		     nextLine=reader.readNext();
		     while ((nextLine = reader.readNext()) != null) {
		        VOStation nuevo=new VOStation(Integer.parseInt(nextLine[0]),nextLine[1], nextLine[2], Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Integer.parseInt(nextLine[5]));
		        listaEstaciones.add(nuevo);
		     }
		     reader.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try{
		CSVReader reader = new CSVReader(new FileReader(tripsFile));
	     String [] nextLine;
	     nextLine=reader.readNext();
		     while ((nextLine = reader.readNext()) != null) {
		    	 VOTrip nuevo=new VOTrip(Integer.parseInt(nextLine[0]),Integer.parseInt(nextLine[3]), Double.parseDouble(nextLine[4]), Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), nextLine[8],nextLine[10]);
		    	 listaTripsQueue.enqueue(nuevo);
		    	 listaTripsStack.push(nuevo);
		     }
		     reader.close();
	     }catch(Exception e){
		     e.printStackTrace();
	     }
	}
	
	@Override
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		// TODO Auto-generated method stub
		DoubleLinkedList<String> lista=new DoubleLinkedList<>();
		int aux=0;
		for(VOTrip nodo:listaTripsQueue){
			if(nodo.getBikeId()==bicycleId){
				aux++;
				lista.add(nodo.getFromStation());
				if(aux==n){
					return lista;
				}
				aux++;
				lista.add(nodo.getToStation());
			}
			if(aux==n){
				return lista;
			}
		}
		return lista;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		// TODO Auto-generated method stub
		int cont = 1;
		for(VOTrip nodo: listaTripsStack)
		{
			if(nodo.getToStationId() == stationID)
			{
				
				if(cont == n)
				{
					return nodo;
				}
				cont++;
				
			}
			
		}
		return null;
	}	


}
