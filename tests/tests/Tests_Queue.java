package tests;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Queue;

public class Tests_Queue {

	private Queue<Integer> cola;
    private int borrado1;
    private int borrado2;
    private int borrado3;

    @Before
    public void setupEscenario1( )
    {
        cola = new Queue<Integer>();
        cola.enqueue(12);

        cola.enqueue(13);
        
        cola.enqueue(123);
    }

    public void setupEscenario2()
    {
        borrado1 = cola.dequeue();
        borrado2 = cola.dequeue();
        borrado3 = cola.dequeue();

    }

    @Test
    public void tests( )
    {
        setupEscenario1();
        assertTrue("La lista esta a�adiendo erroneamente", cola.size() == 3);

        setupEscenario2();
        assertTrue("El elemento no se a�adio en la posicion indicada", borrado1 == 12);
        assertTrue("El elemento no se a�adio en la posicion indicada", borrado2 == 13);
        assertTrue("El elemento no se a�adio en la posicion indicada", borrado3 == 123);
        assertTrue("La cola deber�a estar vac�a", cola.isEmpty() == true);


    }
}
