package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Stack;

public class Tests {

	private Stack<Integer> pila;
    private int borrado1;
    private int borrado2;
    private int borrado3;

    @Before
    public void setupEscenario1( )
    {
        pila = new Stack<Integer>();
        pila.push(12);
        
        pila.push(13);
        
        pila.push(123);
        
    }

    public void setupEscenario2()
    {
        borrado1 = pila.pop();
        borrado2 = pila.pop();
        borrado3 = pila.pop();

    }

    @Test
    public void tests( )
    {
        setupEscenario1();
        assertTrue("La lista esta a�adiendo erroneamente", pila.size() == 3);

        setupEscenario2();
        assertTrue("El elemento no se a�adio en la posicion indicada", borrado1 == 123);
        assertTrue("El elemento no se a�adio en la posicion indicada", borrado2 == 13);
        assertTrue("El elemento no se a�adio en la posicion indicada", borrado3 == 12);
        assertTrue("La pila deber�a estar vac�a", pila.isEmpty() == true);


    }
}
